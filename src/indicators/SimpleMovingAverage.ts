import { Indicator, TradingData } from "../interfaces";

export class SimpleMovingAverage extends Indicator {
  private period: number;
  private values: number[] = [];
  private sum = 0;

  constructor(period: number) {
    super();
    this.period = period;
  }

  update(tradingData: TradingData): void {
    this.values.push(tradingData.close);
    this.sum += tradingData.close;

    if (this.values.length > this.period) {
      this.sum -= this.values.shift()!;
    }
  }

  value(): number | null {
    if (this.values.length < this.period) {
      return null;
    }
    return this.sum / this.period;
  }
}
