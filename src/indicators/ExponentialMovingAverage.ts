import { Indicator, TradingData } from "../interfaces";

export class ExponentialMovingAverage extends Indicator {
  private period: number;
  private values: number[] = [];
  private multiplier: number;
  private ema: number | null = null;

  constructor(period: number) {
    super();
    this.period = period;
    this.multiplier = 2 / (period + 1);
  }

  update(tradingData: TradingData) {
    this.values.push(tradingData.close);

    if (this.values.length < this.period) {
      return;
    }

    if (this.ema === null) {
      const sum = this.values.reduce((a, b) => a + b, 0);
      this.ema = sum / this.period;
    } else {
      this.ema = (tradingData.close - this.ema) * this.multiplier + this.ema;
    }
  }

  value() {
    return this.ema;
  }
}
