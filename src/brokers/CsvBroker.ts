import { TTBar, TTBroker, TTResolution, TTTick, TTTrade } from "../interfaces";

export class CsvBroker implements TTBroker {
  name: "csv broker";
  private trades: TTTrade[];

	constructor(csvFile: string) {
		this.trades = [];
	}

	

  getTick(instrument: string): Promise<TTTick> {
    throw new Error("Method not implemented.");
  }
  getBars(instrument: string, resolution: TTResolution): Promise<TTBar[]> {
    throw new Error("Method not implemented.");
  }
  getHistory(
    instrument: string,
    resolution: TTResolution,
    from: Date,
    to: Date
  ): Promise<TTBar[]> {
    throw new Error("Method not implemented.");
  }
  getBalance(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  getEquity(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  getProfit(): Promise<number> {
    throw new Error("Method not implemented.");
  }
  getOrders(): Promise<TTTrade[]> {
    throw new Error("Method not implemented.");
  }
  getOpenOrders(): Promise<TTTrade[]> {
    throw new Error("Method not implemented.");
  }
  getClosedOrders(): Promise<TTTrade[]> {
    throw new Error("Method not implemented.");
  }
}
