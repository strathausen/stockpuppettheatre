import { TTBar, TTResolution, TTStrategy, TTTrade } from "../interfaces";

export class GridTrader implements TTStrategy {
	instrument: string;
	name: string;
	resolution: TTResolution;
	indicators: {
		emaSlow: {};
		emaFast: {};
		macd: {};
	};
	onBar(bar: TTBar): TTTrade[] {
		return [];
	}
}