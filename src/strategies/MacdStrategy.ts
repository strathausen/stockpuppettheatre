import { TTBar, TTStrategy, TTTrade } from "../interfaces";

export class MacdStrategy implements TTStrategy {
  instrument: "EURUSD";
  name: "MACD";
  resolution: "15m";
  indicators: {
    emaSlow: {};
    emaFast: {};
    macd: {};
  };
  onBar(bar: TTBar): TTTrade[] {
    return [];
  }
}
