export class ExampleStrategy implements TradingStrategy {
  timeframes = [15];
  indicators: Record<string, Indicator> = {
    sma10: new SimpleMovingAverage(10),
    sma20: new SimpleMovingAverage(20),
  };
  private tradingEnvironment: TradingEnvironment;

  constructor(tradingEnvironment: TradingEnvironment) {
    this.tradingEnvironment = tradingEnvironment;
  }

  async onData(): Promise<void> {
    // Implement your strategy logic here, querying the latest indicator values and aggregated data from the TradingEnvironment
  }
}
