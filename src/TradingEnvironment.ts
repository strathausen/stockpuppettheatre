import { DataReader, TradingData, TradingStrategy } from "./interfaces";

export class TradingEnvironment {
  private dataReader: DataReader;
  private strategy: TradingStrategy;

  constructor(dataReader: DataReader, strategy: TradingStrategy) {
    this.dataReader = dataReader;
    this.strategy = strategy;
    this.subscribeToPriceUpdates();
  }

  private subscribeToPriceUpdates() {
    this.dataReader.on("priceUpdate", (tradingData: TradingData) => {
      // Update the indicators
      Object.values(this.strategy.indicators).forEach((indicator) =>
        indicator.update(tradingData)
      );

      // Update the aggregated data
      // (similar to the previous example, but consider storing the aggregated data in a more accessible way)
    });
  }

  // Add methods to query the latest indicator values and aggregated data
}
