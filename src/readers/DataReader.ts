import { TTBar, TTGranularity } from "../interfaces";

export class DataReader {
  on(granularity: TTGranularity, callback: (bar: TTBar) => void): void {}
}
