import { EventEmitter } from 'events';
import { DataReaderEvents } from '../interfaces';

export class BaseReader {
  eventEmitter: EventEmitter;

  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  on<U extends keyof DataReaderEvents>(event: U, listener: DataReaderEvents[U]): this {
    this.eventEmitter.on(event, listener);
    return this;
  }

  off<U extends keyof DataReaderEvents>(event: U, listener: DataReaderEvents[U]): this {
    this.eventEmitter.off(event, listener);
    return this;
  }
}
