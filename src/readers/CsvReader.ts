import { BaseReader } from './BaseReader';
import { TradingData } from "../interfaces";
import * as fs from 'fs';
import * as Papa from 'papaparse';

export class CSVReader extends BaseReader {
  private filePath: string;
  private symbol: string;

  constructor(filePath: string, symbol: string) {
    super();
    this.filePath = filePath;
    this.symbol = symbol;
  }

  async read(): Promise<void> {
    const fileContent = fs.readFileSync(this.filePath, 'utf8');

    // Parse the CSV data
    Papa.parse(fileContent, {
      header: true,
      dynamicTyping: true,
      skipEmptyLines: true,
      complete: (results: any) => {
        if (results.errors.length > 0) {
          console.error('Error parsing CSV:', results.errors);
          return;
        }

        // Emit priceUpdate events
        for (const row of results.data) {
          const tradingData: TradingData = {
            timestamp: new Date(row.timestamp),
            open: row.open,
            high: row.high,
            low: row.low,
            close: row.close,
            volume: row.volume,
            symbol: this.symbol,
          };

          this.eventEmitter.emit('priceUpdate', tradingData);
        }
      },
    });
  }
}
