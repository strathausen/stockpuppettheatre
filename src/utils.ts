export function parseTimeframe(timeframe: string) {
  const units = {
    s: 1000, // seconds
    m: 60 * 1000, // minutes
    h: 60 * 60 * 1000, // hours
    d: 24 * 60 * 60 * 1000, // days
  };

  const regex = /^(\d+)([smhd])$/;
  const match = timeframe.match(regex);

  if (!match) {
    return null;
  }

  const value = parseInt(match[1], 10);
  const unit = units[match[2] as keyof typeof units];

  return value * unit;
}
