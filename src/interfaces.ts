export interface TTBroker {
  name: string;
  getTick(instrument: string): Promise<TTTick>;
  getBars(instrument: string, resolution: TTResolution): Promise<TTBar[]>;
  getHistory(
    instrument: string,
    resolution: TTResolution,
    from: Date,
    to: Date
  ): Promise<TTBar[]>;
  getBalance(): Promise<number>;
  getEquity(): Promise<number>;
  getProfit(): Promise<number>;
  getOrders(): Promise<TTTrade[]>;
  getOpenOrders(): Promise<TTTrade[]>;
  getClosedOrders(): Promise<TTTrade[]>;
}

export interface TTStrategy {
  instrument: string;
  name: string;
  resolution: TTResolution;
  onBar(bar: TTBar): TTTrade[];
}

export interface TTTrade {
  price?: number; // if undefined, then market order
  size: number; // if negative, then sell, if positive, then buy
  comment?: string;
  stopLoss?: number; // for sell orders, this is the lowest price, for buy orders, this is the highest price
  takeProfit?: number; // for sell orders, this is the highest price, for buy orders, this is the lowest price
  openTime?: Date;
  closeTime?: Date;
  closePrice?: number;
}

export interface TTTick {
  time: Date;
  bid: number;
  ask: number;
}

export interface TTBar {
  time: Date;
  open: number;
  high: number;
  low: number;
  close: number;
  volume: number;
}

export type TTResolution =
  | "1m"
  | "5m"
  | "15m"
  | "30m"
  | "1h"
  | "4h"
  | "1d"
  | "5d";

export interface TTDataReader {}

export interface TradingData {
  timestamp: Date;
  symbol: string;
  open: number;
  high: number;
  low: number;
  close: number;
  volume: number;
}

export interface DataReader {}

export interface TradingStrategy {
  timeframes: number[];
  indicators: Record<string, Indicator>;
  onData(): Promise<void>;
}

export abstract class Indicator {
  abstract update(tradingData: TradingData): void;
}

export interface DataReaderEvents {
  priceUpdate: (tradingData: TradingData) => void;
}
